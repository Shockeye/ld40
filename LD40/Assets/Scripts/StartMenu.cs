﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StartMenu : MonoBehaviour
{

	// Use this for initialization
	void Start () {

        //TODO: wake up the Core 
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void QuitGame()
    {
        Core.Instance.QuitGame();
    }

    public void StartGame()
    {
        Core.Instance.StartGame();
    }

    public void Settings()
    {
        //Open settings menu
    }
}
