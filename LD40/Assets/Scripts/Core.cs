﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Core : Singleton<Core>
{
    private KeyCode HotKey_screenshot = KeyCode.F12
        ;
	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(HotKey_screenshot))
        {
            Screenshot();
        }
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void Screenshot()
    {
        //TODO: handle filename increment
        ScreenCapture.CaptureScreenshot("Screenshot.png");
        // With no directory/folder list the image will be written into the Project folder
    }

    public void Pause(bool pause)
    {
        if (pause == true)
        {
            Time.timeScale = 0;

            
            //Update() continues 
            //FixedUpdate() stops being called.The physics engine paused.



        }
        else
        {
            Time.timeScale = 1;

  
        }
    }

    public void StartGame()
    {
        SceneManager.LoadScene("game");
    }

    public void StartMenu()
    {
        SceneManager.LoadScene("start");
    }



}
