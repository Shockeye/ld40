﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class NPC : MonoBehaviour
{
    private Rigidbody body;
    private Animator animator;

    public Transform leader; //this NPC will follow the leader
    bool follow = false; //wont follow unless this is set to true
    private NavMeshAgent agent;
    // Use this for initialization
    void Start()
    {
        body = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        animator.SetBool("idle", true);
        agent = GetComponent<NavMeshAgent>();
        agent.destination = leader.position;

    }

	
	// Update is called once per frame
	void Update ()
    {
        if (Vector3.Distance(transform.position, leader.position) > 2.0f)
        {

            agent.destination = leader.position;
            animator.SetBool("idle", false);
        }
        else
        {
            animator.SetBool("idle", true);
        }

    

    }

    //These guys need to follow the player, and follow simple instructions like stop & go.
}
