﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target;
    public Vector3 offset; //from target
    public float speed; //for lerping


    private Vector3 destination; //desired position


	// Use this for initialization
	void Start ()
    {
        SetDestination();
        transform.position = destination; 
        transform.LookAt(target.transform);
    }
	
	// Update is called once per frame
	void Update ()
    {
        SetDestination();
        transform.position = destination; //TODO: lerp to here
        transform.LookAt(target.transform);
            

    }

    void SetDestination()
    {
        destination = target.transform.rotation * offset;
        destination += target.position;
        
    }

 
}
