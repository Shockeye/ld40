﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float inputDelay = 0.1f;
    public float originHeight; //distance of player models origin from ground
    public LayerMask ground;
    public float forwardVelocity; // todo: run & walk velocities
    public float jumpVelocity;
    public float downwardAcceleration = 9.8f; //gravity - note: terminal velocity needs implementing

    private Vector3 velocity;
    private Rigidbody body;
    private Animator animator;
    private float horizontalInput;
    private float verticalInput;
    private float jumpInput;

    
    
    // Use this for initialization
    void Start ()
    {
        body = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        animator.SetBool("idle", true);

    }
	
	// Update is called once per frame
	void Update ()
    {
        GetInput();
        //float x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
        //float z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;
        float x = horizontalInput * Time.deltaTime * 150.0f;
        transform.Rotate(0, x , 0);
       // transform.Translate(0, 0, z);


    }

    void FixedUpdate()
    {
        
        Move();
        Jump();

        body.velocity = transform.TransformDirection(velocity);
    }

    void GetInput()
    {
        float idleTime = animator.GetFloat("idleTime");
        horizontalInput = Input.GetAxis("Horizontal");// * Time.deltaTime * 150.0f;
        verticalInput = Input.GetAxis("Vertical");// * Time.deltaTime * 3.0f;
        jumpInput = Input.GetAxisRaw("Jump");// * Time.deltaTime * 3.0f;

        if (horizontalInput + verticalInput + jumpInput == 0.0f)
        {
            idleTime += Time.deltaTime;
            animator.SetBool("idle", true);
            
        }
        else
        {
            idleTime = 0.0f;
            animator.SetBool("idle", false);
        }

        animator.SetFloat("idleTime", idleTime);
            
    }

    void Move()
    {
        if (Mathf.Abs(verticalInput) > inputDelay)//gtrthan "delay"
        {
            velocity.z = forwardVelocity * verticalInput;
        }
        else velocity.z = 0;

    }

    void Jump()
    {
        if(jumpInput > 0 && Grounded())
        {
            velocity.y = jumpVelocity;
        }
        else if (jumpInput == 0 && Grounded())
        {
            velocity.y = 0;
        }
        else
        {
            //free fallin'
            velocity.y = -downwardAcceleration;///Time.deltaTime;

        }
    }

    bool Grounded()
    {
        return Physics.Raycast(transform.position, Vector3.down, originHeight, ground);

    }
}
